import pulumi
from pulumi_azure import core, storage, network, compute

# Variables to create the core structure in Azure
az_prefix = "az"
az_suffix = "example"
az_location = "westeurope"

# Variables to create the Virtual Network
az_vn_address = ["192.168.0.0/24", "192.168.1.0/24", "192.168.2.0/24"]
az_vn_dns = ["8.8.8.8", "1.1.1.1"]

# Create an Azure Resource Group
resource_group = core.ResourceGroup(az_prefix + "rg" + az_suffix, 
    name = az_prefix + "rg" + az_suffix,
    location = az_location)

# Create an Azure resource (Storage Account)
account = storage.Account(az_prefix + "sa" + az_suffix,
    name = az_prefix + "sa" + az_suffix,
    resource_group_name = resource_group.name,
    location = resource_group.location,
    account_replication_type = 'LRS',
    account_tier = 'Standard')

# Create an Azure Virtual Network
network01 = network.VirtualNetwork(az_prefix + "vn" + az_suffix,
    name = az_prefix + "vn" + az_suffix,
    resource_group_name = resource_group.name,
    address_spaces = az_vn_address,
    dns_servers = az_vn_dns,
    location=resource_group.location)

# Create some Azure VN Subnets
subnets = []
for i in range(len(az_vn_address)):
    subnets.append(
        network.Subnet(az_prefix + "sn0" + str(i) + az_suffix,
        name = az_prefix + "sn0" + str(i) + az_suffix,
        resource_group_name = resource_group.name,
        opts = pulumi.ResourceOptions(depends_on = [network01]),
        address_prefix = az_vn_address[i],
        virtual_network_name = az_prefix + "vn" + az_suffix)
    )

# Creation of Security Groups and rules
basic_nsg = network.NetworkSecurityGroup(az_prefix + "sgbasic" + az_suffix,
    name = az_prefix + "sgbasic" + az_suffix,
    resource_group_name = resource_group.name,
    location = resource_group.location)

basic_nsg_ssh_in = network.NetworkSecurityRule(az_prefix + "sgbasicssh" + az_suffix,
    name = az_prefix + "sgbasicssh" + az_suffix,
    resource_group_name = resource_group.name,
    access = "Allow",
    description = "Rule to enable SSH from external",
    destination_address_prefix = "*",
    source_address_prefix = "*",
    protocol = "*",
    network_security_group_name = basic_nsg.name,
    direction = "Inbound",
    source_port_range = "*",
    destination_port_range = "22",
    priority = "500")

basic_nsg_ssh_out = network.NetworkSecurityRule(az_prefix + "sgbasicsshout" + az_suffix,
    name = az_prefix + "sgbasicsshout" + az_suffix,
    resource_group_name = resource_group.name,
    access = "Allow",
    description = "Rule to enable SSH from external",
    destination_address_prefix = "*",
    source_address_prefix = "*",
    protocol = "*",
    network_security_group_name = basic_nsg.name,
    direction = "Outbound",
    source_port_range = "*",
    destination_port_range = "22",
    priority = "500")

# Creation of Public IP
public01 = network.PublicIp(az_prefix + "pip01" + az_suffix,
    name = az_prefix + "pip01" + az_suffix,
    resource_group_name = resource_group.name,
    ip_version = "IPv4",
    allocation_method = "Static",
    location = resource_group.location)


# Creating a Network interface in the first subnet
nic_ubuntu01 = network.NetworkInterface(az_prefix + "nic01" + az_suffix,
    name = az_prefix + "nic01" + az_suffix,
    resource_group_name = resource_group.name,
    opts = pulumi.ResourceOptions(depends_on = [subnets[0], public01]),
    ip_configurations = [
        {
            'name': "ubuntu01ipconfig",
            'subnetId': subnets[0].id,
            'privateIpAddressAllocation': "Dynamic",
            'publicIpAddressId': public01.id,
        }
    ],
    dns_servers = az_vn_dns,
    network_security_group_id =  basic_nsg.id,
    location=resource_group.location)


# Create of one Azure Virtual machine
ubuntu01 = compute.VirtualMachine(az_prefix + "vm01" + az_suffix,
    name = az_prefix + "vm01" + az_suffix,
    resource_group_name = resource_group.name,
    opts = pulumi.ResourceOptions(depends_on = [nic_ubuntu01]),
    vm_size = "Standard_B1s",
    network_interface_ids = [ nic_ubuntu01.id ],
    os_profile = {
        'computerName': 'ubuntu01',
        'adminUsername': "pulumi",
        'adminPassword': "pulumipulumi1$",
        },
    os_profile_linux_config = {
        'disablePasswordAuthentication': 'false',
        },
    storage_os_disk = {
        'createOption': "FromImage",
        'name': "ubuntudisk01",
        },
    storage_image_reference = {
        'publisher': "canonical",
        'offer': "UbuntuServer",
        'sku': "18.04-LTS",
        'version': "latest",
        },
    location = resource_group.location)

# Export the connection string for the storage account
#pulumi.export('connection_string', account.primary_connection_string)

# Export the IP CIDR to use it
pulumi.export('ip_address', public01.ip_address)
