This files are related to the post number 58 in the Tangelov.me webpage.

To use the Dockerfile properly you must generate first the Static files from Hugo cloning [this repository](https://gitlab.com/tangelov/go-tangelov-me) and executing ```hugo```

Once we have the static code generated, we can start using the Dockerfile and the other files from this folder.
